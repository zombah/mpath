#!/bin/bash

SLEEPTIME=10

#IP Address or domain name to ping. The script relies on the domain being
#pingable and always available
TESTIP=8.8.8.8

#Ping timeout in seconds
TIMEOUT=2

# External interfaces
EXTIF1=vlan0
EXTIF2=vlan1

#IP address of external interfaces.
IP1=172.16.7.212
IP2=172.16.1.251


# the gateway
GW1=172.16.7.1
GW2=172.16.1.1

# Relative weights of routes. Keep this to a low integer value.
W1=1
W2=2

# Broadband providers name; use your own names here.
NAME1=uplink1
NAME2=unlink2

#No of repeats of success or failure before changing status of connection
SUCCESSREPEATCOUNT=4
FAILUREREPEATCOUNT=1

# Last link status
LLS1=1
LLS2=1

# Last ping status.
LPS1=1
LPS2=1

# Current ping status.
CPS1=1
CPS2=1

# Change link status
CLS1=1
CLS2=1

# Count of repeated up status or down status
COUNT1=0
COUNT2=0

while : ; do
ping -W $TIMEOUT -I $IP1 -c 1 $TESTIP > /dev/null 2>&1
RETVAL=$?

if [ $RETVAL -ne 0 ]; then
logger $NAME1 Down
CPS1=1
else
CPS1=0
fi

if [ $LPS1 -ne $CPS1 ]; then
logger Ping status changed for $NAME1 from $LPS1 to $CPS1
COUNT1=1
else
if [ $LPS1 -ne $LLS1 ]; then
COUNT1=`expr $COUNT1 + 1`
fi
fi

if [[ $COUNT1 -ge $SUCCESSREPEATCOUNT || ($LLS1 -eq 0 && $COUNT1 -ge $FAILUREREPEATCOUNT) ]]; then
logger Uptime status will be changed for $NAME1 from $LLS1
CLS1=0
COUNT1=0
if [ $LLS1 -eq 1 ]; then
LLS1=0
else
LLS1=1
fi
else
CLS1=1
fi

LPS1=$CPS1

ping -W $TIMEOUT -I $IP2 -c 1 $TESTIP > /dev/null 2>&1
RETVAL=$?

if [ $RETVAL -ne 0 ]; then
logger $NAME2 Down
CPS2=1
else
CPS2=0
fi

if [ $LPS2 -ne $CPS2 ]; then
logger Ping status changed for $NAME2 from $LPS2 to $CPS2
COUNT2=1
else
if [ $LPS2 -ne $LLS2 ]; then
COUNT2=`expr $COUNT2 + 1`
fi
fi

if [[ $COUNT2 -ge $SUCCESSREPEATCOUNT || ($LLS2 -eq 0 && $COUNT2 -ge $FAILUREREPEATCOUNT) ]]; then
logger Uptime status will be changed for $NAME2 from $LLS2
CLS2=0
COUNT2=0
if [ $LLS2 -eq 1 ]; then
LLS2=0
else
LLS2=1
fi
else
CLS2=1
fi

LPS2=$CPS2

if [[ $CLS1 -eq 0 || $CLS2 -eq 0 ]]; then
if [[ $LLS1 -eq 1 && $LLS2 -eq 0 ]]; then
logger Switching to $NAME2
ip route replace default scope global via $GW2 dev $EXTIF2
elif [[ $LLS1 -eq 0 && $LLS2 -eq 1 ]]; then
logger Switching to $NAME1
ip route replace default scope global via $GW1 dev $EXTIF1
elif [[ $LLS1 -eq 0 && $LLS2 -eq 0 ]]; then
logger Restoring default load balancing
ip route replace default scope global nexthop via $GW1 dev $EXTIF1 weight $W1 nexthop via $GW2 dev $EXTIF2 weight $W2
fi
fi
sleep $SLEEPTIME
done
exit 0